﻿function generateClassLists() {

    var tableName = "StudentDataTable"; // Table name to use as a reference
    var sheetName = "StudentData"; // Sheet name that contains all student data

    var classList = document.getElementById("courseList");

    Excel.run(function (ctx) {

        // Table reference
        var table = ctx.workbook.tables.getItem(tableName);

        // Column references to be manipulated, and load the, with values
        var nameColumn = table.columns.getItem("Student Name").load("values");
        var classColumn = table.columns.getItem("Class Code").load("values");

        return ctx.sync().then(function () {

            showNotification("Processing", "Generating class lists");

            var classLists = {}; // Table for classes
            var classes = getUniqueValues(classColumn);

            for (var i = 0; i < classes.length; i++) {

                // Create student array for this course
                var students = [];

                for (var j = 1; j < classColumn.values.length; j++) {

                    if (classColumn.values[j][0] === classes[i]) {
                        students.push(nameColumn.values[j][0]);
                    }
                }

                // Add students to hash table in class
                classLists[classes[i]] = students;


                // Add items to list
                var listItem = document.createElement("option");
                var listItemValue = document.createTextNode(classes[i]);

                listItem.appendChild(listItemValue);
                classList.appendChild(listItem);

            }

        })
    }).catch(errorHandler());

}

/**
 * Get a unique list of values from a column
 * @param {any} column - The column as a range that you would like unique values from
 */
function getUniqueValues(column) {

    var uniqueValues = [];
    var values = [];

    // Have to get the value from the 0th column of each row because Microsoft
    for (var i = 0; i < column.values.length; i++) {
        values.push(column.values[i][0]);
    }

    // Loop through and the index is the first occurrence add to the unique list
    for (var i = 1; i < values.length; i++) {
        var value = values[i];

        if (values.indexOf(value) === i) {
            uniqueValues.push(values[i]);
        }
    }

    // Put the name column into a set to get a unique list of names
    return uniqueValues;
}