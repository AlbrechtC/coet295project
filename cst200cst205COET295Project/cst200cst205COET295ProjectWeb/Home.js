﻿(function () {
    "use strict";

    var cellToHighlight;
    var messageBanner;

    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {
            // Initialize the FabricUI notification mechanism and hide it
            var element = document.querySelector('.ms-MessageBanner');
            messageBanner = new fabric.MessageBanner(element);
            messageBanner.hideBanner();

            $('#button-text').text("Generate Student's Sheets");
            $('#button-desc').text("Grabs students from the list, and generates a sheet for each student consisting of their GPA and course marks.");

            $('#goto-sheet-text').text("Go to Sheet");
            $('#goto-sheet-desc').text("Select a sheet from the select-box and then click the button to navigate to the sheet.");
            // Add a click event handler for the highlight button.
            $('#Generate-Student-Sheets').click(generateSheets);
            $('#goto-sheet').click(goToSheet);

            // Add a click event handler for the Generate Class lists button.
            $('#Generate-ClassLists').click(generateClassLists);

            $("#courseList").change(showClass);

            $('#gotoMarks').click(gotoMarks);
            $('#gotoPrint').click(gotoPrint);


        });

        /**
         * Function handles all people picker options, found on the
         * microsoft frabric UI website example.
         * 
         **/
        (function ($) {
            $.fn.PeoplePicker = function () {

                /** Iterate through each people picker provided. */
                return this.each(function () {

                    var $peoplePicker = $(this);
                    var $searchField = $peoplePicker.find(".ms-PeoplePicker-searchField");
                    var $results = $peoplePicker.find(".ms-PeoplePicker-results");
                    var $searchMore = $peoplePicker.find(".ms-PeoplePicker-searchMore");
                    var $selected = $peoplePicker.find('.ms-PeoplePicker-selected');
                    var $selectedPeople = $peoplePicker.find(".ms-PeoplePicker-selectedPeople")
                    var $selectedCount = $peoplePicker.find(".ms-PeoplePicker-selectedCount")
                    var isActive = false;
                    var spinner;

                    // Run when focused or clicked
                    function peoplePickerActive(event) {
                        /** Scroll the view so that the people picker is at the top. */
                        $('html, body').animate({
                            scrollTop: $peoplePicker.offset().top
                        }, 367);

                        /** Start by closing any open people pickers. */
                        if ($('.ms-PeoplePicker').hasClass('is-active')) {
                            $(".ms-PeoplePicker").removeClass("is-active");
                        }

                        isActive = true;

                        /** Stop the click event from propagating, which would just close the dropdown immediately. */
                        event.stopPropagation();

                        /** Before opening, size the results panel to match the people picker. */
                        $results.width($peoplePicker.width() - 2);

                        /** Show the $results by setting the people picker to active. */
                        $peoplePicker.addClass("is-active");

                        /** Temporarily bind an event to the document that will close the people picker when clicking anywhere. */
                        $(document).bind("click.peoplepicker", function (event) {
                            $peoplePicker.removeClass('is-active');
                            $(document).unbind('click.peoplepicker');
                            isActive = false;
                        });
                    };

                    /** Set to active when focusing on the input. */
                    $peoplePicker.on('focus', '.ms-PeoplePicker-searchField', function (event) {
                        peoplePickerActive(event);
                    });

                    /** Set to active when clicking on the input. */
                    $peoplePicker.on('click', '.ms-PeoplePicker-searchField', function (event) {
                        peoplePickerActive(event);
                    });

                    /** Keep the people picker active when clicking within it. */
                    $(this).click(function (event) {
                        event.stopPropagation();
                    });

                    /** Add the selected person to the text field or selected list and close the people picker. */
                    $results.on('click', '.ms-PeoplePicker-result', function (event) {
                        var selectedName = $(this).find(".ms-Persona-primaryText").html();
                        var selectedTitle = $(this).find(".ms-Persona-secondaryText").html();
                        var personaHTML = '<div class="ms-PeoplePicker-persona">' +
                            '<div class="ms-Persona ms-Persona--xs">' +
                            '<div class="ms-Persona-imageArea">' +
                            '<i class="ms-Persona-placeholder ms-Icon ms-Icon--person"></i>' +
                            '<div class="ms-Persona-presence"></div>' +
                            '</div>' +
                            '<div class="ms-Persona-details">' +
                            '<div class="ms-Persona-primaryText">' + selectedName + '</div>' +
                            ' </div>' +
                            '</div>' +
                            '<button class="ms-PeoplePicker-personaRemove">' +
                            '<i class="ms-Icon ms-Icon--x"></i>' +
                            ' </button>' +
                            '</div>';
                        var personaListItem = '<li class="ms-PeoplePicker-selectedPerson">' +
                            '<div class="ms-Persona">' +
                            '<div class="ms-Persona-imageArea">' +
                            '<i class="ms-Persona-placeholder ms-Icon ms-Icon--person"></i>' +
                            '<div class="ms-Persona-presence"></div>' +
                            '</div>' +
                            '<div class="ms-Persona-details">' +
                            '<div class="ms-Persona-primaryText">' + selectedName + '</div>' +
                            '<div class="ms-Persona-secondaryText">' + selectedTitle + '</div>' +
                            '</div>' +
                            '</div>' +
                            '<button class="ms-PeoplePicker-resultAction js-selectedRemove"><i class="ms-Icon ms-Icon--x"></i></button>' +
                            '</li>';
                        if (!$peoplePicker.hasClass('ms-PeoplePicker--facePile')) {
                            $searchField.val(selectedName);
                            $peoplePicker.removeClass("is-active");
                            resizeSearchField($peoplePicker);
                        }
                        else {
                            if (!$selected.hasClass('is-active')) {
                                $selected.addClass('is-active');
                            }
                            $selectedPeople.prepend(personaListItem);
                            $peoplePicker.removeClass("is-active");

                            var count = $peoplePicker.find('.ms-PeoplePicker-selectedPerson').length;
                            $selectedCount.html(count);
                        }
                    });

                    /** Remove the persona when clicking the personaRemove button. */
                    $peoplePicker.on('click', '.ms-PeoplePicker-personaRemove', function (event) {
                        $(this).parents('.ms-PeoplePicker-persona').remove();

                        /** Make the search field 100% width if all personas have been removed */
                        if ($('.ms-PeoplePicker-persona').length == 0) {
                            $peoplePicker.find('.ms-PeoplePicker-searchField').outerWidth('100%');
                        } else {
                            resizeSearchField($peoplePicker);
                        }
                    });
                    /** Remove a result using the action icon. */
                    $results.on('click', '.js-resultRemove', function (event) {
                        event.stopPropagation();
                        $(this).parent(".ms-PeoplePicker-result").remove();
                    });
                });
            };

            function resizeSearchField($peoplePicker) {

                var $searchBox = $peoplePicker.find('.ms-PeoplePicker-searchBox');

                // Where is the right edge of the search box?
                var searchBoxLeftEdge = $searchBox.position().left;
                var searchBoxWidth = $searchBox.outerWidth();
                var searchBoxRightEdge = searchBoxLeftEdge + searchBoxWidth;

                // Where is the right edge of the last persona component?
                var $lastPersona = $searchBox.find('.ms-PeoplePicker-persona:last');
                var lastPersonaLeftEdge = $lastPersona.offset().left;
                var lastPersonaWidth = $lastPersona.outerWidth();
                var lastPersonaRightEdge = lastPersonaLeftEdge + lastPersonaWidth;

                // Adjust the width of the field to fit the remaining space.
                var newFieldWidth = searchBoxRightEdge - lastPersonaRightEdge - 7;

                // Don't let the field get too tiny.
                if (newFieldWidth < 100) {
                    newFieldWidth = "100%";
                }

                $peoplePicker.find('.ms-PeoplePicker-searchField').outerWidth(newFieldWidth);
            }

        })(jQuery);

        if ($.fn.PeoplePicker) {
            $('.ms-PeoplePicker').PeoplePicker();
        }
    };

    function gotoMarks() {
        $('#PrintClassListsPage').hide();
        $('#StudentGradesPage').show();
    }

    function gotoPrint() {
        $('#StudentGradesPage').hide();
        $('#PrintClassListsPage').show();
    }

    function goToSheet() {
        var selectedName = $(".ms-PeoplePicker-searchField").val();

        Excel.run(function (ctx) {

            // Verify a name was selected or entered into the select field
            if (selectedName !== "") {
                var sheet = ctx.workbook.worksheets.getItem(selectedName);
            } else {
                selectedName = "";
            }

            return ctx.sync().then(function () {
                // If a name was selected/entered then proceed to make it the active sheet
                if (selectedName != "") {
                    sheet.activate();
                    showNotification("Success", "You have switch to worksheet: " + selectedName);
                } else {
                    // Log the error
                    showNotification("Error 601", "No sheet has been selected from the select box.");
                }
            })
        }).catch(errorHandler(error));
    }

    function generateSheets() {

        var tableName = "StudentDataTable"; // Table name to use as a reference
        var sheetName = "StudentData"; // Sheet name that contains all student data

        Excel.run(function (ctx) {

            // Table reference
            var table = ctx.workbook.tables.getItem(tableName);
            var sheets = ctx.workbook.worksheets.load("items/name");

            // Column references to be manipulated, and load the, with values
            var nameColumn = table.columns.getItem("Student Name").load("values");
            var markColumn = table.columns.getItem("Mark").load("values");
            var weightColumn = table.columns.getItem("Weight").load("values");
            var classColumn = table.columns.getItem("Class Code").load("values");

            return ctx.sync().then(function () {

                if (sheets.items.length === 1) {
                    showNotification("Success", "Generating sheets for all student's");
                    // Array containing each row/record in the tabl
                    var records = [];

                    // Loop through all rows
                    for (var i = 1; i < nameColumn.values.length; i++) {
                        var curr_name = nameColumn.values[i][0];
                        var curr_mark = markColumn.values[i][0];
                        var curr_weight = weightColumn.values[i][0];
                        var curr_class = classColumn.values[i][0];

                        // Loop through and push each row of data to the array as a JSON object
                        records.push({
                            name: curr_name,
                            mark: curr_mark,
                            weight: curr_weight,
                            class: curr_class
                        });
                    }

                    // Variables to keep track of what sheets have been added
                    var names = [];
                    var addAlready;

                    // Loop through each record in the array and perform actions on them
                    for (var i = 0; i < records.length; i++) {
                        var curr_student = records[i];
                        addAlready = false;

                        for (var j = 0; j < names.length; j++) {
                            if (names[j] === curr_student.name) {
                                addAlready = true;
                            }
                        }
                        if (addAlready === false) {
                            names.push(curr_student.name);
                            handleNewSheet(records, curr_student, ctx);
                        }
                    }
                } else {
                    showNotification("Error 602", "You must only have the StudentData sheet in the workbook.");
                }

            })
        }).catch(errorHandler(error));
    }

    /**
     * Handles the creation of new sheets meaning that the name was found for the first time in the table
     * 
     */
    function handleNewSheet(records, curr_student, ctx) {

        // Add the worksheet to the page and include it in the already added array
        var temp = ctx.workbook.worksheets.add(curr_student.name);
        $(".ms-PeoplePicker-resultList").append("<li class='ms-PeoplePicker-result'><button class='ms-PeoplePicker-resultBtn'><div class='ms-Persona'><div class='ms-Persona-details'> <div class='ms-Persona-primaryText'>" + curr_student.name + "</div> </div> </div></button></li>");

        // Include a header above the table
        var startCell = temp.getRange("B2");
        startCell.values = [["Student: " + curr_student.name]];
        startCell.format.font.bold = true;
        startCell.format.font.size = 14;
        startCell.format.font.color = "#3e3e3e";

        // Table headers
        var categories = ["Mark", "Weight", "Class"];

        // Build the table
        var tableTemp = temp.tables.add("B4:D4", true);
        tableTemp.getHeaderRowRange().values = [categories];

        // Variables to be used in the gpa calculation
        var sumOfWeights = 0;
        var markTotal = 0;
        var valueList = [];

        // Populate the table
        for (var k = 0; k < records.length; k++) {
            if (records[k].name === curr_student.name) {
                tableTemp.rows.add(null, [[records[k].mark * 100 + "%", records[k].weight, records[k].class]])
                markTotal += (records[k].mark * 100) * records[k].weight;
                sumOfWeights += records[k].weight;
                valueList.push(records[k].class);
            }
        }


        // GPA information display
        var gpaDisplay = temp.getRange("B3");
        gpaDisplay.values = [["GPA: " + Math.round(markTotal / sumOfWeights) + "%"]];
        gpaDisplay.format.font.bold = true;
        gpaDisplay.format.font.size = 12;
        gpaDisplay.format.font.color = "#3e3e3e";
    }

    function generateClassLists() {

        var tableName = "StudentDataTable"; // Table name to use as a reference
        var sheetName = "StudentData"; // Sheet name that contains all student data

        var classList = document.getElementById("courseList");



        Excel.run(function (ctx) {

            // Table reference
            var table = ctx.workbook.tables.getItem(tableName);

            // Column references to be manipulated, and load the, with values
            var nameColumn = table.columns.getItem("Student Name").load("values");
            var classColumn = table.columns.getItem("Class Code").load("values");

            return ctx.sync().then(function () {

                showNotification("Processing", "Generating class lists");

                var classLists = {}; // Table for classes
                var classes = getUniqueValues(classColumn);
                var existingClasses = [];

                // If the class list contains the same number of classes as was found 
                //in the spreadsheet assume the classes in the sheet are the same and exit
                if (classList.childElementCount === classes.length + 1) {
                    return;
                }
                else {
                    $("#courseList").each(function () {
                        existingClasses.push($(this).value);
                    });
                }

                for (var i = 0; i < classes.length; i++) {

                    // Create student array for this course
                    var students = [];

                    for (var j = 1; j < classColumn.values.length; j++) {

                        if (classColumn.values[j][0] === classes[i]) {
                            students.push(nameColumn.values[j][0]);
                        }
                    }

                    // Add students to hash table in class
                    classLists[classes[i]] = students;

                    // If class isnt already in the list
                    if (existingClasses.indexOf(classes[i]) === -1) {
                        // Add items to list
                        var listItem = document.createElement("option");

                        listItem.value = classes[i];

                        var listItemValue = document.createTextNode(classes[i]);

                        listItem.appendChild(listItemValue);
                        classList.appendChild(listItem);

                        // Add items to preview
                        var preview = document.getElementById("courseListPreview");

                        var tableItem = document.createElement("table");
                        tableItem.id = "print" + classes[i];

                        preview.appendChild(tableItem);

                        $("#print" + classes[i]).hide();

                        var tableCap = document.createElement("caption");
                        var tableCapVal = document.createTextNode(classes[i]);

                        tableCap.appendChild(tableCapVal);

                        tableItem.appendChild(tableCap);

                        var tableBody = document.createElement("body");

                        tableItem.appendChild(tableBody);

                        for (var z = 0; z < classLists[classes[i]].length; z++) {
                            var tableRow = document.createElement("tr");
                            tableBody.appendChild(tableRow);

                            var tableData = document.createElement("td");
                            var tableDataValue = document.createTextNode(classLists[classes[i]][z]);
                            tableData.appendChild(tableDataValue);
                            tableRow.appendChild(tableData);
                        }

                    }
                }

            });
        }).catch(errorHandler());

    }

    function showClass() {
        var course = $("#courseList").val();
        $("#print" + course).show();
    }

    /**
     * Print the list
     * Unfortunately this does not work in edge
     * @param {any} thingToPrint - The div to print
     */
    function printList() {
        var course = $("#courseList").val();
        var list = $("#print" + course).html();
        page = window.open();
        page.document.write(contents);
        page.print();
        page.close();
    }

    /**
     * Get a unique list of values from a column
     * @param {any} column - The column as a range that you would like unique values from
     * @returns {any} - Array of unique items from the passed in column
     */
    function getUniqueValues(column) {

        var uniqueValues = [];
        var values = [];

        // Have to get the value from the 0th column of each row because Microsoft
        for (var i = 0; i < column.values.length; i++) {
            values.push(column.values[i][0]);
        }

        // Loop through and the index is the first occurrence add to the unique list
        for (var i = 1; i < values.length; i++) {
            var value = values[i];

            if (values.indexOf(value) === i) {
                uniqueValues.push(values[i]);
            }
        }

        // Put the name column into a set to get a unique list of names
        return uniqueValues;
    }

    // Helper function for treating errors
    function errorHandler(error) {
        // Always be sure to catch any accumulated errors that bubble up from the Excel.run execution
        showNotification("Error", error);
        console.log("Error: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    }

    // Helper function for displaying notifications
    function showNotification(header, content) {
        $("#notification-header").text(header);
        $("#notification-body").text(content);
        messageBanner.showBanner();
        messageBanner.toggleExpansion();
    }
})();
